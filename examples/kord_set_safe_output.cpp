#include <chrono>
#include <cmath>
#include <csignal>
#include <iomanip>
#include <iostream>

#include <kord/api/kord.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord_io_request.h>
#include <kord/api/kord_receive_interface.h>
#include <kord/utils/utils.h>

using namespace kr2;
volatile bool g_run = true;

std::map<uint, std::string> CONFIG2NAME{{0, "Disabled"},
                                        {1, "Disabled"},
                                        {2, "Enabled"},
                                        {3, "PStop"},
                                        {4, "EStop"},
                                        {5, "EStop+PStop"}};

void signal_handler(int a_signum)
{
    psignal(a_signum, "[KORD-API]");
    g_run = false;
}

int main(int argc, char *argv[])
{
    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv);

    if (lp.help_ || !lp.valid_) {
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler);
    signal(SIGTERM, signal_handler);

    if (lp.useRealtime()) {
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)) {
            std::cerr << "Failed to start with realtime priority\n";
            kr2::utils::LaunchParameters::printUsage(true);
            return EXIT_FAILURE;
        }
    }

    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    std::shared_ptr<kord::KordCore> kord(
        new kord::KordCore(lp.remote_controller_, lp.port_, lp.session_id_, kord::UDP_CLIENT));

    kord::ControlInterface ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);

    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }

    g_run = true;

    // Obtain initial q values
    if (!kord->syncRC()) {
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";
    rcv_iface.fetchData();
    std::array<double, 7UL> start_q = rcv_iface.getJoint(kord::ReceiverInterface::EJointValue::S_ACTUAL_Q);

    std::cout << "Read initial joint configuration:\n";
    for (double angl : start_q)
        std::cout << (angl / 3.14) * 180 << " ";
    std::cout << "\n";

    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    if (!kord->waitSync(std::chrono::milliseconds(10))) {
        std::cout << "Sync wait timed out, exit \n";
        return EXIT_FAILURE;
    }

    // Create a request to the remote controller
    kr2::kord::RequestIO io_request;
    io_request.asSetIODigitalOut().withEnabledSafePorts(kr2::kord::RequestIO::DIGITAL_SAFE::SDO1,
                                                        ESafePortConfiguration::eSafePortBothMapped);

    kord->sendCommand(io_request);
    std::cout << "TX Request    RID: " << io_request.request_rid_ << "\n";

    // We can try setting EStop or PStop and check whether digital output changes
    int64_t last_digital_output = -1;
    uint32_t last_safe_do_config = -1;
    kr2::kord::Request response;
    while (g_run) {
        kord->waitSync(std::chrono::milliseconds(10));
        rcv_iface.fetchData();

        int64_t digital_output = rcv_iface.getDigitalOutput();
        if (last_digital_output != digital_output) {
            std::cout << "DO: " << rcv_iface.getDigitalOutput() << std::endl;
            last_digital_output = digital_output;
        }

        uint32_t safe_do_config = rcv_iface.getSafeDigitalOutputConfig();

        if (last_safe_do_config != safe_do_config) {
            std::cout << "SDO1 config: " << CONFIG2NAME[static_cast<uint32_t>((safe_do_config >> 24) & 0xFF)] << "\n";
            std::cout << "SDO2 config: " << CONFIG2NAME[static_cast<uint32_t>((safe_do_config >> 16) & 0xFF)] << "\n";
            std::cout << "SDO3 config: " << CONFIG2NAME[static_cast<uint32_t>((safe_do_config >> 8) & 0xFF)] << "\n";
            std::cout << "SDO4 config: " << CONFIG2NAME[static_cast<uint32_t>((safe_do_config) & 0xFF)] << "\n";
            last_safe_do_config = safe_do_config;
        }

        response = rcv_iface.getLatestRequest();

        if (static_cast<uint32_t>((safe_do_config >> 24) & 0xFF) == 5) {
            std::cout << "SUCCESS: Request with RID " << response.request_rid_ << "\n";
            break;
        }

        if (lp.runtimeElapsed()) {
            std::cout << "TIMEOUT: Request with RID " << response.request_rid_ << "\n";
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}
