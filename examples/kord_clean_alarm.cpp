#include <chrono>
#include <cmath>
#include <csignal>
#include <iomanip>
#include <iostream>

#include <kord/api/kord.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord_receive_interface.h>
#include <kord/utils/utils.h>

using namespace kr2;
volatile bool stop = false;

void signal_handler(sig_atomic_t a_signum)
{
    psignal(a_signum, "[KORD-API]");
    stop = true;
}

std::map<kord::ControlInterface::EClearRequest, std::string> enum2str{
    {kord::ControlInterface::EClearRequest::CBUN_EVENT, "CBUN_EVENT"},
    {kord::ControlInterface::EClearRequest::CLEAR_HALT, "CLEAR_HALT"},
    {kord::ControlInterface::EClearRequest::UNSUSPEND, "UNSUSPEND"},
    {kord::ControlInterface::EClearRequest::CONTINUE_INIT, "CONTINUE_INIT"},
};

int main(int argc, char *argv[])
{
    struct ExtraOptions {
        bool clear_all = false;
        kord::ControlInterface::EClearRequest command_to_RC = kord::ControlInterface::EClearRequest::CLEAR_HALT;
    } extras;
    static kr2::utils::LaunchParameters::ExternalArgParser ep = [argc, &argv, &extras](int index) -> void {
        static kr2::utils::SOALongOptions ex_options{
            std::array<kr2::utils::LongOption,
                       5>{kr2::utils::LongOption{{"all", no_argument, nullptr, 'a'}, "clear all errors"},
                          kr2::utils::LongOption{{"halt", no_argument, nullptr, 'm'}, "clear halt on the controller"},
                          kr2::utils::LongOption{{"unsuspend", no_argument, nullptr, 'u'}, "unsuspend the robot"},
                          kr2::utils::LongOption{{"init", no_argument, nullptr, 'n'},
                                                 "continue robot initialization in case it was blocked"},
                          kr2::utils::LongOption{{"cbun", no_argument, nullptr, 'b'},
                                                 "acknowledge CBun error and clear it"}}};

        if (index <= kr2::utils::LaunchParameters::INVALID_INDEX) {
            std::cout << ex_options.helpString() << "\n";
            return;
        }

        int option_index = 0;
        optind = index;
        int opt = getopt_long(argc, argv, "amunb", ex_options.getLongOptions(), &option_index);

        switch (opt) {
        case 'a':
            extras.clear_all = true;
            break;
        case 'm':
            extras.command_to_RC = kord::ControlInterface::EClearRequest::CLEAR_HALT;
            break;
        case 'u':
            extras.command_to_RC = kord::ControlInterface::EClearRequest::UNSUSPEND;
            break;
        case 'n':
            extras.command_to_RC = kord::ControlInterface::EClearRequest::CONTINUE_INIT;
            break;
        case 'b':
            extras.command_to_RC = kord::ControlInterface::EClearRequest::CBUN_EVENT;
            break;
        default:
            std::cout << "Unknown option found" << " optidx: " << optind << ", argc: " << argc << "\n";
            exit(EXIT_FAILURE);
        }
    };

    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv, ep);

    if (lp.help_ || !lp.valid_) {
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler);

    if (lp.useRealtime()) {
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)) {
            std::cerr << "Failed to start with realtime priority\n";
            kr2::utils::LaunchParameters::printUsage(false);
            return EXIT_FAILURE;
        }
    }

    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    std::shared_ptr<kord::KordCore> kord(
        new kord::KordCore(lp.remote_controller_, lp.port_, lp.session_id_, kord::UDP_CLIENT));

    kord::ControlInterface ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);

    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }

    if (!kord->syncRC()) {
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";

    std::vector<kord::ControlInterface::EClearRequest> commands;
    if (extras.clear_all) {
        commands = {kord::ControlInterface::EClearRequest::CLEAR_HALT,
                    kord::ControlInterface::EClearRequest::CBUN_EVENT,
                    kord::ControlInterface::EClearRequest::CONTINUE_INIT,
                    kord::ControlInterface::EClearRequest::UNSUSPEND};
    }
    else {
        commands = {extras.command_to_RC};
    }

    for (const auto &command : commands) {
        int64_t token = ctl_iface.clearAlarmRequest(command);

        std::cout << enum2str[command] << " command sent with token: " << token << '\n';
        while (rcv_iface.getCommandStatus(token) == -1) {
            if (!kord->waitSync(std::chrono::milliseconds(10), kord::F_SYNC_FULL_ROTATION)) {
                std::cout << "Sync wait timed out, exit \n";
                break;
            }

            rcv_iface.fetchData();
            if (stop) {
                break;
            }
        }
        auto status = rcv_iface.getCommandStatus(token);
        std::cout << enum2str[command] << " command status: " << signed(status) << '\n';
    }

    return 0;
}
