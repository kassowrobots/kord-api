#include <chrono>
#include <cmath>
#include <csignal>
#include <iomanip>
#include <iostream>

#include <kord/api/kord.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord_receive_interface.h>
#include <kord/utils/utils.h>

using namespace kr2;
volatile bool g_run = true;

void signal_handler(int a_signum)
{
    psignal(a_signum, "[KORD-API]");
    g_run = false;
}

int main(int argc, char *argv[])
{
    struct ExtraOptions {
        std::optional<std::vector<int>> to_lock{};
        std::optional<std::vector<int>> to_unlock{};
        bool engage_mode = true;
    } extras;
    static kr2::utils::LaunchParameters::ExternalArgParser ep = [argc, &argv, &extras](int index) -> void {
        static kr2::utils::SOALongOptions ex_options{
            std::array<kr2::utils::LongOption,
                       2>{kr2::utils::LongOption{{"engage", optional_argument, nullptr, 'e'},
                                                 "lock the specified joint, locks all joints if none provided",
                                                 "<1,2,3,4,5,6,7>"},
                          kr2::utils::LongOption{{"disengage", optional_argument, nullptr, 'd'},
                                                 "unlock the specified joints, unlocks all joints if none provided",
                                                 "<1,2,3,4,5,6,7>"}}};

        if (index <= kr2::utils::LaunchParameters::INVALID_INDEX) {
            std::cout << ex_options.helpString() << "\n";
            return;
        }

        int option_index = 0;
        optind = index;
        int opt = getopt_long(argc, argv, "e:d:", ex_options.getLongOptions(), &option_index);

        switch (opt) {
        case 'e': {
            extras.engage_mode = true;
            std::stringstream ss(optarg);
            std::string item;
            std::vector<double> values;

            while (std::getline(ss, item, ',')) {
                values.push_back(std::stod(item));
            }

            extras.to_lock.emplace();
            extras.to_lock->resize(values.size());
            std::copy(values.begin(), values.end(), extras.to_lock->begin());
            break;
        }
        case 'd': {
            extras.engage_mode = false;
            std::stringstream ss(optarg);
            std::string item;
            std::vector<double> values;

            while (std::getline(ss, item, ',')) {
                values.push_back(std::stod(item));
            }

            extras.to_unlock.emplace();
            extras.to_unlock->resize(values.size());
            std::copy(values.begin(), values.end(), extras.to_unlock->begin());
            break;
        }
        default: {
            std::cout << "Unknown option found" << " optidx: " << optind << ", argc: " << argc << "\n";
            exit(EXIT_FAILURE);
        }
        }
    };

    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv, ep);

    if (lp.help_ || !lp.valid_) {
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler);

    if (lp.useRealtime()) {
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)) {
            std::cerr << "Failed to start with realtime priority\n";
            kr2::utils::LaunchParameters::printUsage(false);
            return EXIT_FAILURE;
        }
    }

    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    auto kord = std::make_shared<kord::KordCore>(lp.remote_controller_, lp.port_, lp.session_id_, kord::UDP_CLIENT);

    kord::ControlInterface ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);

    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }

    g_run = true;

    // Obtain initial q values
    if (!kord->syncRC()) {
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";
    rcv_iface.fetchData();
    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    while (g_run) {
        // Wait for the heartbeat
        if (!kord->waitSync(std::chrono::milliseconds(10))) {
            std::cout << "Sync wait timed out, exit \n";
            break;
        }

        // When heartbeat was captured, transmit the request - brake joints 5,6,7
        int64_t token;

        if (extras.engage_mode) {
            std::cout << "Engaging brakes...\n";
            if (!extras.to_lock.has_value())
                ctl_iface.engageBrakes({1, 2, 3, 4, 5, 6, 7}, token);
            else
                ctl_iface.engageBrakes(extras.to_lock.value(), token);
        }
        else {
            std::cout << "Disengaging brakes...\n";
            if (!extras.to_unlock.has_value())
                ctl_iface.disengageBrakes({1, 2, 3, 4, 5, 6, 7}, token);
            else
                ctl_iface.disengageBrakes(extras.to_unlock.value(), token);
        }

        if (!kord->waitSync(std::chrono::milliseconds(20))) {
            std::cout << "Sync wait timed out, exit \n";
            break;
        }

        std::cout << "Command sent with token: " << token << '\n';
        while (rcv_iface.getCommandStatus(token) == -1) {
            if (!g_run) {
                break;
            }

            if (!kord->waitSync(std::chrono::milliseconds(20))) {
                std::cout << "Sync wait timed out, exit \n";
                break;
            }

            rcv_iface.fetchData();
        }
        int8_t status = rcv_iface.getCommandStatus(token);
        std::cout << "Command status: " << signed(status) << '\n';

        // Currently there is no feedback to check whether the brakes were really engaged.
        break;
    }

    return 0;
}
