#include <cmath>
#include <iomanip>
#include <iostream>

#include <kord/api/api_request.h>
#include <kord/api/kord.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord_receive_interface.h>
#include <kord/utils/utils.h>

#include "kr2/kord/system/RobotControllerFlags.h"

#include <chrono>
#include <csignal>
#include <sstream>

using namespace kr2;
using namespace kr2::kord::protocol;
volatile bool stop = false;

void signal_handler(sig_atomic_t a_signum)
{
    psignal(a_signum, "[KORD-API]");
    stop = true;
}

int main(int argc, char *argv[])
{
    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv);

    if (lp.help_ || !lp.valid_) {
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler);

    if (lp.useRealtime()) {
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)) {
            std::cerr << "Failed to start with realtime priority\n";
            lp.printUsage(false);
            return EXIT_FAILURE;
        }
    }

    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    std::shared_ptr<kord::KordCore> kord(
        new kord::KordCore(lp.remote_controller_, lp.port_, lp.session_id_, kord::UDP_CLIENT));

    kord::ControlInterface ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);

    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }

    if (!kord->syncRC()) {
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";

    rcv_iface.fetchData();

    if (rcv_iface.getHWFlags() & HW_STAT_INIT_RUID_MISMATCH) {
        std::cout << "In fetch data state, sending the command...\n";
        if (!kord->waitSync(std::chrono::milliseconds(100))) {
            std::cout << "Sync wait timed out, exit \n";
        }
        kr2::kord::RequestRCAPICommand sys_request;
        sys_request.asUserConsent().addPayload(kr2::kord::protocol::ERCAPIPayloadCmdConsentId::eSkipFetch);
        ctl_iface.transmitRequest(sys_request);
    }

    return 0;
}
