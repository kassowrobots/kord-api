//
//  - robot !!!MUST!!! be standstill to transfer log files
//  - this test sends request to retrieve logs from the remote controller
//  - the target and the username must be defined in KORD.ini
//

#include <cmath>
#include <iomanip>
#include <iostream>

#include <kord/api/api_request.h>
#include <kord/api/kord.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord_receive_interface.h>
#include <kord/utils/utils.h>

#include <chrono>
#include <csignal>
#include <sstream>

using namespace kr2;
static bool g_run = true;

//! Will request the KORD Cbun to initiate and transfer log files to target
int64_t requestMoreFilesTransfer(kord::KordCore &, kord::ControlInterface &);

//! Will monitor the status of the request to tranfer logs until time out, failure, or success
bool monitorTransfer(kord::KordCore &, kord::ReceiverInterface &, utils::LaunchParameters &, int64_t);

void signal_handler(int a_signum)
{
    psignal(a_signum, "[KORD-API]");
    g_run = false;
}

int main(int argc, char *argv[])
{
    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv);

    if (lp.help_ || !lp.valid_) {
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler);

    if (lp.useRealtime()) {
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)) {
            std::cerr << "Failed to start with realtime priority\n";
            kr2::utils::LaunchParameters::printUsage(false);
            return EXIT_FAILURE;
        }
    }

    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    std::shared_ptr<kord::KordCore> kord(
        new kord::KordCore(lp.remote_controller_, lp.port_, lp.session_id_, kord::UDP_CLIENT));

    kord::ControlInterface ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);

    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }

    g_run = true;

    // Obtain initial q values
    if (!kord->syncRC()) {
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";
    rcv_iface.fetchData();
    std::array<double, 7UL> start_q = rcv_iface.getJoint(kord::ReceiverInterface::EJointValue::S_ACTUAL_Q);

    std::cout << "Read initial joint configuration:\n";
    for (double angl : start_q)
        std::cout << (angl / 3.14) * 180 << " ";
    std::cout << "\n";

    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    // Initiate the execution of log transfer
    int64_t request_id = 0;
    request_id = requestMoreFilesTransfer(*kord, ctl_iface);
    if (request_id <= 0) {
        std::cout << "Failed to sent Transfer request\n";
        return EXIT_FAILURE;
    }

    // Waiting for the execution result
    if (!monitorTransfer(*kord, rcv_iface, lp, request_id)) {
        std::cout << "Failure\n";
        return EXIT_FAILURE;
    }

    std::cout << "Done\n";
    return EXIT_SUCCESS;
}

int64_t requestMoreFilesTransfer(kord::KordCore &a_kord, kord::ControlInterface &a_ctl_iface)
{
    if (!a_kord.waitSync(std::chrono::milliseconds(10))) {
        std::cout << "Sync wait timed out, exit \n";
        return -1;
    }

    // Create a request to the remote controller
    kr2::kord::RequestTransfer transfer_request;
    // sys_request.asDataTranfer().withDahsboardJSon().wasDashboardJsonTransfer();
    // combine with parameters
    transfer_request.asFilesTransfer().withDashboardJson().withCalibration();

    if (!a_ctl_iface.transmitRequest(transfer_request)) {
        return -2;
    }

    std::cout << "TX Request    RID: " << transfer_request.request_rid_ << "\n";
    return transfer_request.request_rid_;
}

bool monitorTransfer(kord::KordCore &a_kord,
                     kord::ReceiverInterface &a_rcv_iface,
                     utils::LaunchParameters &a_lp,
                     int64_t a_req_id)
{
    kr2::kord::Request response;
    while (g_run) {
        a_kord.waitSync(std::chrono::milliseconds(10));
        a_rcv_iface.fetchData();

        response = a_rcv_iface.getLatestRequest();

        if (a_lp.runtimeElapsed()) {
            std::cout << "TIMEOUT: Request with RID " << response.request_rid_ << "\n";
            return false;
        }

        // Check if the correct request is being evaluated
        if (a_req_id != response.request_rid_) {
            continue;
        }

        if (response.request_status_ == kr2::kord::protocol::EControlCommandStatus::eSuccess) {
            std::cout << "SUCCESS: Request with RID " << response.request_rid_ << ", transfer finished.\n";
            return true;
        }

        if (response.request_status_ == kr2::kord::protocol::EControlCommandStatus::eFailure) {
            std::cout << "FAILURE: Request with RID: " << response.request_rid_ << "\n";
            std::cout << "               error code: " << response.error_code_ << "\n";
            return false;
        }
    }

    std::cout << "Monitor transfer was interrupted\n";
    return false;
}
