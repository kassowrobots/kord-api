#include <chrono>
#include <cmath>
#include <csignal>
#include <iostream>

#include "kord/utils/utils.h"
#include <kord/api/kord.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord_receive_interface.h>

using namespace kr2;

volatile bool stop = false;

void printVector(const std::vector<std::variant<double, int>> &vec, const std::string &label)
{
    std::cout << label << " [ ";
    for (const auto &val : vec) {
        std::cout << std::get<double>(val) << " ";
    }
    std::cout << "]" << std::endl;
}

template <typename T, size_t N> void printArray(const std::array<T, N> &vec, const std::string &label)
{
    std::cout << label << " [ ";
    for (auto val : vec) {
        std::cout << val << " ";
    }
    std::cout << "]" << std::endl;
}

void signal_handler(sig_atomic_t a_signum)
{
    psignal(a_signum, "[KORD-API]");
    stop = true;
}

int main(int argc, char *argv[])
{
    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv);

    if (lp.help_ || !lp.valid_) {
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler);

    if (lp.useRealtime()) {
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)) {
            std::cerr << "Failed to start with realtime priority\n";
            kr2::utils::LaunchParameters::printUsage(true);
            return EXIT_FAILURE;
        }
    }

    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    std::shared_ptr<kord::KordCore> kord(
        new kord::KordCore(lp.remote_controller_, lp.port_, lp.session_id_, kord::UDP_CLIENT));

    kord::ControlInterface ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);

    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }

    std::vector<double> q(7);

    // Obtain initial q values
    if (!kord->syncRC()) {
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }

    rcv_iface.fetchData();
    auto start_q = rcv_iface.getJoint(kord::ReceiverInterface::EJointValue::S_ACTUAL_Q);
    std::array<double, 7> degrees{};
    std::transform(start_q.begin(), start_q.end(), degrees.begin(), [](double angl) {
        return (angl * 180.0) / M_PI;
    });
    auto sensed_trq = rcv_iface.getJoint(kord::ReceiverInterface::EJointValue::S_SENSED_TRQ);
    auto tcp_to_wf = rcv_iface.getFrame(kord::EFrameID::TCP_FRAME, kord::EFrameValue::POSE_VAL_REF_WF);
    auto tcp_fo_tfc = rcv_iface.getFrame(kord::EFrameID::TCP_FRAME, kord::EFrameValue::POSE_VAL_REF_TFC);
    auto tfc_to_wf = rcv_iface.getFrame(kord::EFrameID::TFC_FRAME, kord::EFrameValue::POSE_VAL_REF_WF);
    auto tfc_fo_tfc = rcv_iface.getFrame(kord::EFrameID::TFC_FRAME, kord::EFrameValue::POSE_VAL_REF_TFC);
    auto tcp_quat = rcv_iface.getTCPWithQuaternion();

    auto tcp = rcv_iface.getTCP();

    printArray(start_q, "Radians:");
    printArray(degrees, "Degrees:");
    printArray(sensed_trq, "Sensed torques:");
    printVector(tcp_to_wf, "TCP to WF:");
    printVector(tcp_fo_tfc, "TCP to TFC");
    printVector(tfc_to_wf, "TFC to WF:");
    printVector(tfc_fo_tfc, "TFC to TFC:");
    printArray(tcp, "TCP:");
    printArray(tcp_quat, "TCP quaternion:");

    auto start = std::chrono::steady_clock::now();
    std::cout << "Runtime: "
              << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count()
              << "\n";
    std::cout << "Safety flags: " << rcv_iface.getRobotSafetyFlags() << "\n";

    std::cout << kr2::utils::SystemAlarmStateDecoder::decodeAsString(rcv_iface.systemAlarmState());
    std::cout << "HW flags: " << rcv_iface.getHWFlags() << "\n";
    std::cout << "Button flags: " << rcv_iface.getButtonFlags() << "\n";
    std::cout << "Motion flags: " << rcv_iface.getMotionFlags() << "\n";
    std::cout << "Safety  mode: " << rcv_iface.getSafetyMode() << "\n";
    std::cout << "Master speed: " << rcv_iface.getMasterSpeed() << "\n";
    std::cout << "Runtime, SF, SSTOP; PSTOP, ESTOP\n";
    auto print_stamp = std::chrono::time_point_cast<std::chrono::seconds>(std::chrono::steady_clock::now());

    std::array<double, 7UL> acs_joints = rcv_iface.getJoint(kord::ReceiverInterface::EJointValue::S_ACTUAL_QDD);
    std::cout << "Joints accelerations: [ ";
    for (double ac : acs_joints)
        std::cout << ac << " ";
    std::cout << "]" << std::endl;

    auto trq_dev = rcv_iface.getJoint(kord::ReceiverInterface::EJointValue::S_TRQDEV_SMOOTH);
    std::cout << "Torque deviation: [ ";
    for (double td : trq_dev)
        std::cout << td << " ";
    std::cout << "]" << std::endl;

    std::cout << std::endl;
    std::cout << "FailEmpty: " << rcv_iface.getStatistics(kord::ReceiverInterface::EStatsValue::FAIL_TO_READ_EMPTY)
              << "\n";
    std::cout << "FailError: " << rcv_iface.getStatistics(kord::ReceiverInterface::EStatsValue::FAIL_TO_READ_ERROR)
              << "\n";
    return 0;
}
