#include <chrono>
#include <csignal>
#include <iostream>
#include <memory>

#include <kord/api/api_request.h>
#include <kord/api/kord.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord_receive_interface.h>
#include <kord/utils/utils.h>

#include "kr2/kord/protocol/ServerParameters.h"
#include "kr2/kord/protocol/ServerServiceStatus.h"
#include "kr2/kord/system/RobotControllerFlags.h"

using namespace kr2;
using namespace kr2::kord::protocol;
volatile bool stop = false;

void signal_handler(sig_atomic_t a_signum)
{
    psignal(a_signum, "[KORD-API]");
    stop = true;
}

int main(int argc, char *argv[])
{
    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv);

    if (lp.help_ || !lp.valid_) {
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler);

    if (lp.useRealtime()) {
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)) {
            std::cerr << "Failed to start with realtime priority\n";
            kr2::utils::LaunchParameters::printUsage(false);
            return EXIT_FAILURE;
        }
    }

    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    auto kord = std::make_shared<kord::KordCore>(lp.remote_controller_, lp.port_, lp.session_id_, kord::UDP_CLIENT);

    // insert code here...
    kord::ControlInterface ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);

    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }

    uint64_t flag;

    if (!kord->syncRC()) {
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";

    rcv_iface.fetchData();

    kord::RequestSystem request;
    request.asServerCommunication(true);
    ctl_iface.transmitRequest(request);

    if (!kord->waitSync(std::chrono::milliseconds(100))) {
        std::cout << "Sync wait timed out, exit \n";
    }
    rcv_iface.fetchData();

    // Need to wait some time so the connection is established
    std::this_thread::sleep_for(std::chrono::seconds(2));

    if (rcv_iface.getHWFlags() & HW_STAT_INIT_RUID_MISMATCH) {
        auto parameters = std::make_shared<ServiceFetchKincalParameters>(true);
        auto req = kord::RequestServer().asServiceRequest(EServerServiceCommands::eStart,
                                                          EKORDServerServiceID::eFetchKincalData,
                                                          parameters);

        if (!kord->waitSync(std::chrono::milliseconds(100))) {
            std::cout << "Sync wait timed out, exit \n";
        }
        rcv_iface.fetchData();

        ctl_iface.transmitRequest(req);
        std::cout << "Kincal fetch start was sent successfully\n";
        // Sleep to prevent request from being overridden
        std::this_thread::sleep_for(std::chrono::seconds(3));
    }

    if (!kord->waitSync(std::chrono::milliseconds(100))) {
        std::cout << "Sync wait timed out, exit \n";
    }
    rcv_iface.fetchData();

    auto status_req = kord::RequestServer().asServiceRequest(EServerServiceCommands::eGetStatus,
                                                             EKORDServerServiceID::eFetchKincalData);

    // Wait til finished
    int idle_threshold = 20;
    int idle_counter = 0;
    while (!stop) {
        if (!kord->waitSync(std::chrono::milliseconds(100))) {
            std::cout << "Sync wait timed out, exit \n";
            break;
        }
        if (idle_counter > idle_threshold) {
            stop = true;
            break;
        }

        ctl_iface.transmitRequest(status_req);
        rcv_iface.fetchData();
        flag = rcv_iface.getServerResponseFlag();

        auto status = static_cast<uint64_t>((flag >> 9) & 0xFF);
        auto progress = static_cast<uint64_t>((flag >> 1) & 0xFF);

        std::cout << "Progress: " << unsigned(progress) << ", status: " << unsigned(status) << std::endl;
        if (status == static_cast<uint8_t>(EServiceStatus::eSuccess)) {
            std::cout << "Success" << std::endl;
            stop = true;
        }
        else if (status == static_cast<uint8_t>(EServiceStatus::eFailed)) {
            std::cout << "Failed" << std::endl;
            stop = true;
        }
        else if (status == static_cast<uint8_t>(EServiceStatus::eIdle)) {
            std::cout << "Service is not running" << std::endl;
            idle_counter++;
            stop = true;
        }
    }

    // Stop service
    auto stop_req =
        kord::RequestServer().asServiceRequest(EServerServiceCommands::eStop, EKORDServerServiceID::eFetchKincalData);
    ctl_iface.transmitRequest(stop_req);

    if (!kord->waitSync(std::chrono::milliseconds(100))) {
        std::cout << "Sync wait timed out, exit \n";
        return EXIT_FAILURE;
    }

    // Disable python interpreter
    request.asServerCommunication(false);
    ctl_iface.transmitRequest(request);

    if (!kord->waitSync(std::chrono::milliseconds(100))) {
        std::cout << "Sync wait timed out, exit \n";
        return EXIT_FAILURE;
    }

    return 0;
}
