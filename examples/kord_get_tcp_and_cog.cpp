#include <chrono>
#include <cmath>
#include <csignal>
#include <iostream>

#include <kord/api/kord.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord_receive_interface.h>

using namespace kr2;

volatile bool stop = false;

void signal_handler(sig_atomic_t a_signum)
{
    psignal(a_signum, "[KORD-API]");
    stop = true;
}

bool areVectorsSame(const std::vector<std::variant<double, int>> &vec1,
                    const std::vector<std::variant<double, int>> &vec2)
{
    return vec1 == vec2;
}

void printVector(const std::vector<std::variant<double, int>> &vec, const std::string &label)
{
    std::cout << label << " [ ";
    for (const auto &val : vec) {
        std::cout << std::get<double>(val) << " ";
    }
    std::cout << "]" << std::endl;
}

int main(int argc, char *argv[])
{
    struct ExtraOptions {
        bool whitelist_enabled = false;
        bool print_tcp = false;
        bool print_tfc = false;
        bool print_load1 = false;
        bool print_load2 = false;
    } extras;
    static kr2::utils::LaunchParameters::ExternalArgParser ep = [argc, &argv, &extras](int index) -> void {
        static kr2::utils::SOALongOptions ex_options{std::array<kr2::utils::LongOption, 4>{
            kr2::utils::LongOption{{"tcp", no_argument, nullptr, 'n'}, "print TCP and TCP pose in TFC", ""},
            kr2::utils::LongOption{{"tfc", no_argument, nullptr, 'm'}, "print TFC", ""},
            kr2::utils::LongOption{{"load1", no_argument, nullptr, 'k'}, "print LOAD1", ""},
            kr2::utils::LongOption{{"load2", no_argument, nullptr, 'l'}, "print LOAD2", ""},
        }};

        if (index <= kr2::utils::LaunchParameters::INVALID_INDEX) {
            std::cout << ex_options.helpString() << "\n";
            return;
        }

        int option_index = 0;
        optind = index;
        int opt = getopt_long(argc, argv, "ed", ex_options.getLongOptions(), &option_index);

        switch (opt) {
        case 'n': {
            extras.whitelist_enabled = true;
            extras.print_tcp = true;
            break;
        }
        case 'm': {
            extras.whitelist_enabled = true;
            extras.print_tfc = true;
            break;
        }
        case 'k': {
            extras.whitelist_enabled = true;
            extras.print_load1 = true;
            break;
        }
        case 'l': {
            extras.whitelist_enabled = true;
            extras.print_load2 = true;
            break;
        }
        default: {
            std::cout << "Unknown option found" << " optidx: " << optind << ", argc: " << argc << "\n";
            exit(EXIT_FAILURE);
        }
        }
    };

    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv, ep);

    if (lp.help_ || !lp.valid_) {
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler);

    if (lp.useRealtime()) {
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)) {
            std::cerr << "Failed to start with realtime priority\n";
            kr2::utils::LaunchParameters::printUsage(false);
            return EXIT_FAILURE;
        }
    }

    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    std::shared_ptr<kord::KordCore> kord(
        new kord::KordCore(lp.remote_controller_, lp.port_, lp.session_id_, kord::UDP_CLIENT));

    kord::ControlInterface ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);

    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }

    std::vector<double> q(7);

    // Obtain initial q values
    if (!kord->syncRC()) {
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";
    rcv_iface.fetchData();
    std::array<double, 6UL> fetched_TCP = rcv_iface.getTCP();

    if (!kord->syncRC()) {
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";
    rcv_iface.fetchData();
    fetched_TCP = rcv_iface.getTCP();

    if (!kord->syncRC()) {
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";
    rcv_iface.fetchData();
    fetched_TCP = rcv_iface.getTCP();

    long counter = 0;

    // refresh frequency
    std::vector<std::variant<double, int>> prev_tcp;
    std::vector<std::variant<double, int>> prev_tfc;
    std::vector<std::variant<double, int>> prev_tcp_wrt_tfc;
    std::vector<std::variant<double, int>> prev_load1_cog;
    std::vector<std::variant<double, int>> prev_load1_mass;
    std::vector<std::variant<double, int>> prev_load1_inertia;
    std::vector<std::variant<double, int>> prev_load2_cog;
    std::vector<std::variant<double, int>> prev_load2_mass;
    std::vector<std::variant<double, int>> prev_load2_inertia;

    while (!stop) {
        counter++;

        if (!kord->waitSync(std::chrono::milliseconds(10), kord::F_SYNC_FULL_ROTATION)) {
            std::cout << "Sync RC failed.\n";
            return EXIT_FAILURE;
        }

        rcv_iface.fetchData();
        auto tcp_array = rcv_iface.getTCP();
        auto tcp = std::vector<std::variant<double, int>>{tcp_array.begin(), tcp_array.end()};
        auto tfc = rcv_iface.getFrame(kord::EFrameID::TFC_FRAME, kord::EFrameValue::POSE_VAL_REF_WF);
        auto tcp_wrt_tfc = rcv_iface.getFrame(kord::EFrameID::TCP_FRAME, kord::EFrameValue::POSE_VAL_REF_TFC);

        auto load1_cog = rcv_iface.getLoad(kord::ELoadID::LOAD1, kord::ELoadValue::COG_VAL);
        auto load1_mass = rcv_iface.getLoad(kord::ELoadID::LOAD1, kord::ELoadValue::MASS_VAL);
        auto load1_inertia = rcv_iface.getLoad(kord::ELoadID::LOAD1, kord::ELoadValue::INERTIA_VAL);

        auto load2_cog = rcv_iface.getLoad(kord::ELoadID::LOAD2, kord::ELoadValue::COG_VAL);
        auto load2_mass = rcv_iface.getLoad(kord::ELoadID::LOAD2, kord::ELoadValue::MASS_VAL);
        auto load2_inertia = rcv_iface.getLoad(kord::ELoadID::LOAD2, kord::ELoadValue::INERTIA_VAL);

        // Check if any vector has changed
        bool anyChange = !areVectorsSame(tcp, prev_tcp) || !areVectorsSame(tfc, prev_tfc) ||
                         !areVectorsSame(tcp_wrt_tfc, prev_tcp_wrt_tfc) || !areVectorsSame(load1_cog, prev_load1_cog) ||
                         !areVectorsSame(load1_mass, prev_load1_mass) ||
                         !areVectorsSame(load1_inertia, prev_load1_inertia) ||
                         !areVectorsSame(load2_cog, prev_load2_cog) || !areVectorsSame(load2_mass, prev_load2_mass) ||
                         !areVectorsSame(load2_inertia, prev_load2_inertia);

        if (anyChange) {
            // Update previous vectors
            prev_tcp = tcp;
            prev_tfc = tfc;
            prev_tcp_wrt_tfc = tcp_wrt_tfc;
            prev_load1_cog = load1_cog;
            prev_load1_mass = load1_mass;
            prev_load1_inertia = load1_inertia;
            prev_load2_cog = load2_cog;
            prev_load2_mass = load2_mass;
            prev_load2_inertia = load2_inertia;

            if (!extras.whitelist_enabled) {
                // Print all vectors
                printVector(tcp, "TCP:");
                printVector(tfc, "TFC Pose in WF:");
                printVector(tcp_wrt_tfc, "TCP Pose in TFC:");
                printVector(load1_cog, "Load1.cog =");
                printVector(load1_mass, "Load1.mass =");
                printVector(load1_inertia, "Load1.inertia =");
                printVector(load2_cog, "Load2.cog =");
                printVector(load2_mass, "Load2.mass =");
                printVector(load2_inertia, "Load2.inertia =");
                continue;
            }

            if (extras.print_tcp) {
                printVector(tcp, "TCP:");
                printVector(tcp_wrt_tfc, "TCP Pose in TFC:");
            }
            if (extras.print_tfc) {
                printVector(tfc, "TFC Pose in WF:");
            }
            if (extras.print_load1) {
                printVector(load1_cog, "Load1.cog =");
                printVector(load1_mass, "Load1.mass =");
                printVector(load1_inertia, "Load1.inertia =");
            }
            if (extras.print_load2) {
                printVector(load2_cog, "Load2.cog =");
                printVector(load2_mass, "Load2.mass =");
                printVector(load2_inertia, "Load2.inertia =");
            }

            std::cout << "\n";
        }
    }

    return 0;
}
