#include <chrono>
#include <cmath>
#include <csignal>
#include <iostream>
#include <thread>

#include <kord/api/kord.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord_receive_interface.h>
#include <kord/utils/utils.h>

#include "ncurses.h"

using namespace kr2;
static std::atomic<bool> g_run = true;

std::atomic<double> speed;

void signal_handler(int a_signum)
{
    psignal(a_signum, "[KORD-API]");
    g_run = false;
}

void read_keys()
{
    initscr();            // Initialize the ncurses library
    raw();                // Disable line buffering
    keypad(stdscr, TRUE); // Enable special keys, like arrow keys
    noecho();             // Do not display pressed keys
    timeout(10);          // Set a timeout of 50 milliseconds

    printw("\nPress left and right arrow keys to enable self-motion. Press 'q' to quit.");

    int ch;
    int not_pressed_counter = 0;
    double current_speed = 0; // Track the current speed to avoid unnecessary updates

    while (g_run) {
        ch = getch();
        if (ch != ERR) {
            switch (ch) {
            case KEY_LEFT: {
                current_speed = -0.5;
                not_pressed_counter = 0;
                break;
            }
            case KEY_RIGHT: {
                current_speed = 0.5;
                not_pressed_counter = 0;
                break;
            }
            case 'q': {
                g_run = false;
                break;
            }
            default: {
                not_pressed_counter += 1;
                break;
            }
            }
        }
        else {
            // No key is pressed
            not_pressed_counter += 1;
        }

        // Update speed only if a key is pressed; otherwise, reset speed to 0
        if (not_pressed_counter > 50)
            speed = 0;
        else
            speed = current_speed;

        refresh();
    }

    endwin(); // End the ncurses mode
}

int main(int argc, char *argv[])
{
    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv);

    if (lp.help_ || !lp.valid_) {
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler);

    if (lp.useRealtime()) {
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)) {
            std::cerr << "Failed to start with realtime priority\n";
            kr2::utils::LaunchParameters::printUsage(true);
            return EXIT_FAILURE;
        }
    }

    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    std::shared_ptr<kord::KordCore> kord(
        new kord::KordCore(lp.remote_controller_, lp.port_, lp.session_id_, kord::UDP_CLIENT));

    kord::ControlInterface ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);

    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }

    g_run = true;

    // Obtain initial q values
    if (!kord->syncRC()) {
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }

    std::cout << "Sync Captured \n";
    rcv_iface.fetchData();
    if (rcv_iface.systemAlarmState()) {
        // notify alarm
        std::cout << "System alart state - cannot continue\n";
        return EXIT_FAILURE;
    }

    std::array<double, 7UL> start_q = rcv_iface.getJoint(kord::ReceiverInterface::EJointValue::S_ACTUAL_Q);

    std::cout << "Read initial joint configuration:\n";
    for (double angl : start_q)
        std::cout << (angl / 3.14) * 180 << " ";
    std::cout << "\n";

    std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();

    auto read_thread = std::thread([]() { read_keys(); });

    while (g_run) {
        if (!kord->waitSync(std::chrono::milliseconds(10))) {
            std::cout << "Sync wait timed out, exit \n";
            break;
        }
        ctl_iface.moveManifold(speed);

        rcv_iface.fetchData();
        if (rcv_iface.systemAlarmState() || lp.runtimeElapsed()) {
            break;
        }
    }

    std::cout << "Robot stopped\n";
    std::cout << rcv_iface.getFormattedInputBits() << std::endl;
    std::cout << rcv_iface.getFormattedOutputBits() << std::endl;
    std::cout << "SystemAlarmState:\n";
    std::cout << rcv_iface.systemAlarmState() << "\n";
    switch (rcv_iface.systemAlarmState() & 0b1111) {
    case kr2::kord::protocol::EEventGroup::eUnknown:
        std::cout << "No alarms" << '\n';
        break;
    case kr2::kord::protocol::EEventGroup::eSafetyEvent:
        std::cout << "Safety Event" << '\n';
        break;
    case kr2::kord::protocol::EEventGroup::eSoftStopEvent:
        std::cout << "Soft Stop Event" << '\n';
        break;
    case kr2::kord::protocol::EEventGroup::eKordEvent:
        std::cout << "Kord Event" << '\n';
        break;
    }

    std::cout << "Safety flags: " << rcv_iface.getRobotSafetyFlags() << "\n";
    std::cout
        << "Runtime: "
        << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count() /
               1000.0
        << " [s]\n";
    std::cout << "SafetyFlags: " << rcv_iface.getRobotSafetyFlags() << "\n";
    std::cout << "MotionFlags: " << rcv_iface.getMotionFlags() << "\n";
    std::cout << "RCState: " << rcv_iface.getRobotSafetyFlags() << "\n";

    kord->printStats(rcv_iface.getStatisticsStructure());

    read_thread.join();
    return 0;
}
