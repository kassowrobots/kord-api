#include <chrono>
#include <cmath>
#include <csignal>
#include <iomanip>
#include <iostream>

#include <kord/api/kord.h>
#include <kord/api/kord_control_interface.h>
#include <kord/api/kord_receive_interface.h>
#include <kord/utils/utils.h>

using namespace kr2;
static bool g_run = true;

void signal_handler(int a_signum)
{
    psignal(a_signum, "[KORD-API]");
    g_run = false;
}

int main(int argc, char *argv[])
{
    kr2::utils::LaunchParameters lp = kr2::utils::LaunchParameters::processLaunchArguments(argc, argv);

    if (lp.help_ || !lp.valid_) {
        return EXIT_SUCCESS;
    }

    signal(SIGINT, signal_handler);

    if (lp.useRealtime()) {
        if (!kr2::utils::realtime::init_realtime_params(lp.rt_prio_)) {
            std::cerr << "Failed to start with realtime priority\n";
            kr2::utils::LaunchParameters::printUsage(false);
            return EXIT_FAILURE;
        }
    }

    std::cout << "Connecting to: " << lp.remote_controller_ << ":" << lp.port_ << "\n";
    std::cout << "[KORD-API] Session ID: " << lp.session_id_ << std::endl;

    std::shared_ptr<kord::KordCore> kord(
        new kord::KordCore(lp.remote_controller_, lp.port_, lp.session_id_, kord::UDP_CLIENT));

    kord::ControlInterface ctl_iface(kord);
    kord::ReceiverInterface rcv_iface(kord);

    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }

    if (!kord->syncRC()) {
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }
    std::cout << "Sync Captured \n";
    rcv_iface.fetchData();
    std::vector<std::variant<double, int>> returned_vec_start_tcp =
        rcv_iface.getFrame(kord::EFrameID::TCP_FRAME, kord::EFrameValue::POSE_VAL_REF_WF);
    std::array<double, 6UL> start_tcp{};
    for (size_t i = 0; i < start_tcp.size(); ++i) {
        start_tcp[i] = std::get<double>(returned_vec_start_tcp[i]);
    }

    for (double &p : start_tcp)
        std::cout << p << ";";
    std::cout << std::endl;

    // Send 8 velocity control requests
    for (int i = 0; i < 4; i++) {
        std::array<double, 6UL> vel_target = {-0.05, 0.0, 0.0, 0.0, 0.0, 0.0};
        ctl_iface.moveV(vel_target, 0, 0.2, 1);
        usleep(20000);

        vel_target = {0.05, 0.0, 0.0, 0.0, 0.0, 0.0};
        ctl_iface.moveV(vel_target, 0, 0.2, 1);
        usleep(20000);
    }

    // Send velocity control termination
    std::array<double, 6UL> terminate_target = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    ctl_iface.moveV(terminate_target, 0, 0, 0);

    return 0;
}
