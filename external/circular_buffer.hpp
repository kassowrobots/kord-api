#ifndef CIRCULAR_BUFFER_HPP
#define CIRCULAR_BUFFER_HPP

#include <cstddef>
#include <algorithm>
#include <vector>

template <typename T>
class CircularBuffer {
public:
    explicit CircularBuffer(size_t capacity) : capacity_(capacity), size_(0), head_(0) {
        buffer_.resize(capacity_);
    }

    void push_back(const T& value) {
        if (size_ < capacity_) {
            buffer_[(head_ + size_) % capacity_] = value;
            ++size_;
        } else {
            buffer_[head_] = value;
            head_ = (head_ + 1) % capacity_;
        }
    }

    bool empty() const {
        return size_ == 0;
    }

    size_t size() const {
        return size_;
    }

    size_t capacity() const {
        return capacity_;
    }

    const T& operator[](size_t index) const {
        if (index >= size_) {
            throw std::out_of_range("Index out of range");
        }
        return buffer_[(head_ + index) % capacity_];
    }

    void clear() {
        size_ = 0;
        head_ = 0;
    }

    template <typename Predicate>
    bool find_if(Predicate pred, T& foundValue) const {
        auto it = std::find_if(begin(), end(), pred);
        if (it != end()) {
            foundValue = *it;
            return true;
        }
        return false;
    }

    using iterator = typename std::vector<T>::iterator;
    using const_iterator = typename std::vector<T>::const_iterator;

    iterator begin() {
        return buffer_.begin() + head_;
    }

    iterator end() {
        return buffer_.begin() + head_ + size_;
    }

    const_iterator begin() const {
        return buffer_.begin() + head_;
    }

    const_iterator end() const {
        return buffer_.begin() + head_ + size_;
    }

private:
    std::vector<T> buffer_;
    size_t capacity_;
    size_t size_;
    size_t head_;
};

#endif // CIRCULAR_BUFFER_HPP
