
.. _installation-description:

************
Installation
************

Running the KORD requires two parts - KORD CBun on the robot controller and the KORD-API on the client.

.. note::
    
    CBun for customers only. 
    
.. note::

    Real-time is possible with RT capable kernels. With RT kernel you can run examples for streamed movement 
    as real time processes. It helps to reduce the non-deterministic behavior of the client side.

KORD CBun
=========

Needs to be installed, network set properly and activated. It is expected that the robot cabinet is directly 
connected to the client, without any intermediate network elements. If possible, avoid USB to ethernet adapters.

To get the CBun check the robot CBuns manager. If the latest KORD CBun version is not available in the robot,
download the `KORD CBun <https://gitlab.com/kassowrobots/kord-api/-/wikis/Master-CBun>`_ from the wiki page.

Then install it to the robot by using the CBun Manager.


KORD-API
========

Getting the Source
------------------

The KORD-API is compiled from source code. The binaries are not provided.

.. note::
    The compilation was tested under Ubuntu *bionic*, *focal*, and *jammy*. We assume the build environment is already set up.

In order to get the source code you may refer to the `README.md <https://gitlab.com/kassowrobots/kord-api/-/tree/master?ref_type=heads>`_.

The project contains submodules and therefore use of the ``--recurse-submodules``.

.. code-block:: shell
    
    git clone --recurse-submodules https://gitlab.com/kassowrobots/kord-api.git

The directory ``kord-api`` should exist after running the command.

Compilation
-----------

Switch to the ``master`` branch.
To compile the KORD-API enter the cloned directory. Following example will work since ``cmake`` version 3.22.1.

.. code-block:: bash

    $ cd kord-api
    $ cmake -S . -B build
    $ cmake --build build

The compiled output is located in the build directory including examples to run.

Package
-------

If you like, you can build a debian package by entering the build directory and running:

.. code-block:: bash

    $ make package

As a result a single package with the kord library will be created, or two packages library and examples:

.. code-block:: bash

    $ make package
    Run CPack packaging tool...
    CPack: Create package using DEB
    CPack: Install projects
    CPack: - Run preinstall target for: kord_api
    CPack: - Install project: kord_api []
    CPack: -   Install component: examples
    CPack: -   Install component: lib
    CPack: Create package
    -- CPACK_DEBIAN_PACKAGE_DEPENDS not set, the package will have no dependencies.
    -- CPACK_DEBIAN_PACKAGE_DEPENDS not set, the package will have no dependencies.
    CPack: - package: /home/devuser/projects/kord-api-devel/build/kord_api-1.4.5-Linux-examples.deb generated.
    CPack: - package: /home/devuser/projects/kord-api-devel/build/kord_api-1.4.5-Linux-lib.deb generated.


Installation
------------

If you like to install the library to your system, you can run:

.. code-block:: bash

    $ sudo make install

In case you generated the packages you can install them by running:

.. code-block:: bash

    $ sudo dpkg -i kord_api-1.4.5-Linux-lib.deb
    $ sudo dpkg -i kord_api-1.4.5-Linux-examples.deb

Usage
-----

After installation you can use KORD-API in your projects by including it using a cmake find package like this:

.. code-block:: cmake

    find_package(kord_api REQUIRED)

    add_executable(<your_target> <your_source_files>)

    target_link_libraries(<your_target> PRIVATE kord_api::kord_api)
    target_include_directories(<your_target> PRIVATE ${kord_api_INCLUDE_DIRS}) 

.. warning::
    Please notice the KORD-API will not work if you do not have the CBun installed on the robot.

