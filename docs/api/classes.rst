Classes 
-------
   
kord::KordCore
~~~~~~~~~~~~~~
.. doxygenclass:: kr2::kord::KordCore 
  :members:
   
.. doxygenenum:: kr2::kord::EFrameID

.. doxygenenum:: kr2::kord::EFrameValue

.. doxygenenum:: kr2::kord::ELoadID

.. doxygenenum:: kr2::kord::ELoadValue

kord::ReceiverInterface
~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenclass:: kr2::kord::ReceiverInterface 
  :members:
   

kord::Connection_interface
~~~~~~~~~~~~~~~~~~~~~~~~~~
.. doxygenclass:: kr2::kord::Connection_interface
   :members:

kord::ControlInterface
~~~~~~~~~~~~~~~~~~~~~~
.. doxygenclass:: kr2::kord::ControlInterface
   :members:

.. doxygenenum:: kr2::kord::TrackingType

.. doxygenenum:: kr2::kord::BlendType

.. doxygenenum:: kr2::kord::OverlayType

.. doxygenenum:: kr2::kord::ControlInterface::EClearRequest


