API reference
=============

.. toctree::
    :maxdepth: 2
    
    classes
    events
    flags
    appendix