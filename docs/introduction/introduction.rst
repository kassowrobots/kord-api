************
Introduction
************

The KORD operates in the client <-> server manner. The client (controller) is a device intended to control the robot, e.g. laptop. CBun is a "server" counter part executed on the robot side.

To connect to the robot it is advised to use direct wired connection between the controlling device and the robot. Use the static IP address on both devices and make sure both devices are in the same network segment.

The robot has by default the static IP address set to the "192.168.39.1". It is recommended to keep this setting on the robot and choose appropriate IP address of the client/controller, e.g. "192.168.39.5".

Initiating the Control Session
==============================

To initiate a control session over the KORD, the client must send an ``ArmStatus`` request to the CBun/robot. After the CBun successfully captures of the request, it starts transmitting the arm status to the remote controller in regular intervals of 4ms.

This status serves the purpose of synchronization of the remote client/controller to mark the moment for command transmission.

.. note::
    Currently, the arm status dissipation happens every 4ms and it is aligned with the beginning of the controller update loop. 
    There can be only one controller and the status will be transmitted only to the device that initiated the communication.

.. code-block:: c++
    
    // Create and instance of the KordCore - this serves for handling RX/TX KORD frames.
    std::shared_ptr<kord::KordCore> kord(new kord::KordCore("192.168.39.1", 7582, kord::UDP_CLIENT));
    
    // Connect creates the socket and initializes the defaults.
    // Internal timer is initiated to match the robot control loop frequency.
    if (!kord->connect()) {
        std::cout << "Connecting to KR failed\n";
        return EXIT_FAILURE;
    }    

    // Transmit the ArmStatus request to initiate the control session.
    // After this is passed successfully, the status data can be fetched via
    // the receive interface and commands can be send via the control interface.
    if (!kord->syncRC()){
        std::cout << "Sync RC failed.\n";
        return EXIT_FAILURE;
    }


Getting Started
===============

KORD CBun
---------

The CBun needs to be provided with the port number to listen to for incoming connections, and real time priority. There is also a switch to disable RT entirely, which is currently only intended for testing and debug purposes.


KORD API
--------

Compile from source code as it is described in the :ref:`Installation section <installation-description>`.

Minimal Example
===============

The following code will initiate the control session, get the joints positions, prints them on the command line and exits.

It is assumed the robot has an IP address of "192.168.39.1", KORD CBun is activated. The following example does not run in real time mode. It is not critical to run in real time to retrieve the data.

.. code-block:: c++

    #include <kord/api/kord_receive_interface.h>
    #include <kord/api/kord_control_interface.h>

    using namespace kr2;

    int main()
    {                
        std::shared_ptr<kord::KordCore> kord(new kord::KordCore("192.168.39.1", 7582, kord::UDP_CLIENT));

        kord::ControlInterface ctl_iface(kord);
        kord::ReceiverInterface rcv_iface(kord);

        if (!kord->connect()) {
            std::cout << "Connecting to KR failed\n";
            return EXIT_FAILURE;
        }
        
        // Obtain initial q values
        if (!kord->syncRC()){
            std::cout << "Sync RC failed.\n";
            return EXIT_FAILURE;
        }

        std::cout << "Sync Captured \n";
        rcv_iface.fetchData();
        std::array<double, 7UL> start_q = rcv_iface.getJoint(kord::ReceiverInterface::EJointValue::S_ACTUAL_Q);

        std::cout << "Read initial joint configuration:\n";
        for( double angl: start_q )
            std::cout << (angl/3.14)*180 << " ";

        std::cout << "\n";
    }


