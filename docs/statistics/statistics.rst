.. _statistics:

**********
Statistics
**********

.. |br| raw:: html

   <br />


The receive interface provides some basic connection monitoring metrics. 
It is calculated within the CBun and transferred to the KORD API. 
Additionally, the KORD API receiver evaluates capture times of heartbeat 
arm status frame.

Currently, it is only sensible to evaluate the connection quality for joint
and linear movement commands. The movement commands are fed to the CBun
at the rate of 250Hz and we know it should arrive once every 4ms.

CBun Statistics
===============

The items in Table 1 can be retrieved from ``ReceiverInterface::getStatistics(EStatsValue)``.
Those are the statistics calculated at the CBun side and transferred to the API side.

.. list-table:: Table 1: Statistics calculated at the CBun, transferred via Robot Status Heart Beat.
   :widths: 50 65
   :header-rows: 1

   * - Enum Member `EStatsValue`
     - Description
   * - CMD_JITTER_MAX_LOCAL
     - Maximum deviation in command synchronicity within a given |br| window, measured in microseconds.
   * - CMD_JITTER_AVG_LOCAL
     - Average deviation in command synchronicity within a given  |br| window, measured in microseconds.
   * - CMD_JITTER_MAX_GLOBAL
     - Maximum deviation in command synchronicity across the |br| entire operation, measured in microseconds.
   * - ROUND_TRIP_TIME_MAX_LOCAL
     - Maximum round-trip time for a command within a given |br| window, measured in microseconds.
   * - ROUND_TRIP_TIME_AVG_LOCAL
     - Average round-trip time for a command within a given |br| window, measured in microseconds.
   * - ROUND_TRIP_TIME_MAX_GLOBAL
     - Maximum round-trip time for a command overall runtime, |br| measured in microseconds.
   * - CMD_LOST_COUNTER_LOCAL
     - Number of commands lost within a given window. |br| It is based on Sequence numbers from commands.
   * - CMD_LOST_COUNTER_GLOBAL
     - Total number of commands lost throughout the CBun runtime. |br| It is this based on Sequence numbers from commands.
   * - SYS_JITTER_MAX_LOCAL
     - Maximum deviation of the control thread's wake-up time |br| from its scheduled time within a window, |br| measured in microseconds.
   * - SYS_JITTER_AVG_LOCAL
     - Average deviation of the control thread's wake-up time |br| from its scheduled time within a window, |br| measured in microseconds.
   * - SYS_JITTER_MAX_GLOBAL
     - Maximum global deviation of the control thread's wake-up |br| time from its scheduled time throughout the entire operation, |br| measured in microseconds.


.. _api-statistics:

API Statistics
==============

The following items can be retrieved from ``KordCore::getAPIStatistics(EAPIStatistics)`` in order
to assess the connection quality.

.. list-table:: Table 2: Locally calculated statistics on the KORD API side.
   :widths: 50 65
   :header-rows: 1

   * - Enum Member `EAPIStatistics`
     - Description
   * - RSHB_JITTER_MAX_LOCAL
     - Maximum jitter of the Robot Status Heart Beat |br| 
       calculated from the KORD API RX timestamps |br|
       within the specified window.
   * - RSHB_JITTER_MIN_LOCAL
     - Minimum jitter of the Robot Status Heart Beat |br| 
       calculated from the KORD API RX timestamps |br|
       within the specified window.
   * - RSHB_JITTER_AVG_LOCAL
     - Average jitter of the Robot Status Heart Beat |br| 
       calculated from the KORD API RX timestamps |br|
       within the specified window.
   * - RSHB_JITTER_AVG_GLOBAL
     - Average jitter of the Robot Status Heart Beat |br| 
       calculated from the KORD API RX timestamps |br|
       over total runtime.
   * - RSHB_JITTER_MAX_GLOBAL
     - Maximum jitter of the Robot Status Heart Beat |br| 
       calculated from the KORD API RX timestamps |br|
       over total runtime.
   * - RSHB_CONS_LOST_COUNTER_MAX_LOCAL
     - Maximum number of consecutive Robot Status |br| 
       Heart Beat frames lost in the specified window. |br| 
       It is based on sequence numbers from RSHB.
   * - RSHB_CONS_LOST_COUNTER_AVG_LOCAL
     - Average number of consecutive Robot Status |br| 
       Heart Beat frames lost in the specified window. |br| 
       It is based on sequence numbers from RSHB.
   * - RSHB_CONS_LOST_COUNTER_MAX_GLOBAL
     - Maximum number of consecutive Robot Status |br| 
       Heart Beat frames lost over total runtime. |br|
       It is based on sequence numbers from RSHB.
   * - RSHB_LOST_COUNTER_LOCAL
     - Total number of Robot Status Heart Beat  |br|
       frames lost within specified window. |br|
       It is based on sequence numbers from RSHB.




Communication Schemas
=====================

The following sections describe how the statistics are calculated and what stamps 
and events are taken into account.


Start of communication
----------------------

.. figure::  ../images/statistics/CommStart.png
   :align: center

   Figure 1: Communication start procedure (initiation of the communication).


As it is apparent from the image above, the communication must be initiated 
from the KORD API by using the ``syncRC()`` call. This needs to be done only 
one time. After this CBun on the controller side will start with **Robot 
Status Heart Beat** message dissemination. 

Since that moment the communication or continuous movement commands should be 
issued only after calling ``waitSync()``. When ``waitSync()`` releases 
then the commands should be send immediately. Closing this in a loop will
provide a synchronized stream of commands.

Continuous stream of commands allows calculation of the statistics on the side
of the CBun. The statistics provided can then be retrieved from the ``KordCore`` 
or from ``ReceiverInterface``.

There are metrics calculated over a time based on a window width as specified in 
the KORD.ini configuration file. Over the time lapse of the window maxima, minima,
and average is calculated. Along the local statistics, there are also global ones.
Local statistics are intended to be used for a connection quality monitoring.

.. math::
   d = T[i] - T[i-1]
   
**Legend:** 

:math:`d`    
    The delay between consecutive frames.
:math:`T[i]`
    Capture time of a frame at tick `i`.
:math:`T[i-1]`
    Capture time of a frame in previous tick `i-1`.


KORD CBun
---------

The CBun calculates statistics from capture and processing time stamps as it is shown
in the following figure.


.. figure::  ../images/statistics/CBunMeasures.png
   :align: center

   Figure 2: Time stamps and statistics calculated on the CBun side.

Calculations use following equations.

.. math::
   d = T_{CMD\_RX}[i] - T_{CMD\_RX}[i-1]
   
**Legend:** 

:math:`d`    
    The delay between consecutive commands.
:math:`T_{CMD\_RX}[i]`
    The reception time of the current command.
:math:`T_{CMD\_RX}[i-1]`
    The reception time of the previous command.


.. math::
   J_{CMD} = d - T_{KORD}

**Legend:**

:math:`J_{CMD}`
    The jitter of the commands.
:math:`d`
    The delay between consecutive commands.
:math:`T_{KORD}`
    The expected KORD update period (4ms).

To calculate the round trip time we use rough estimation.

Measured data from our tests resulted in the ethernet travel time over 
a 1m unshielded ethernet cable to be about 80us.

.. math::
   t_{RT} = T_{CMD\_RX}[i] - T_{TICK}[i]

**Legend:**

:math:`t_{RT}`
    Round Trip Time of the command.
:math:`T_{CMD\_RX}[i]`
    The reception time of the current command.
:math:`T_{TICK}[i]`
    The start time of the current tick.

.. figure::  ../images/statistics/CBun_Tablet_output.png
   :align: center

   Figure 3: Example of statistics displayed on the tablet.


KORD API
--------

KORD API calculates statistics from reception of the **Robot Status Heart Beat (RSHB)** frame.
Synchronicity of the RSHB is a metric of connection quality. If there is a connection break up
and the frame does not arrive anymore, the statistics cannot be updated. 

.. figure::  ../images/statistics/APIMeasures.png
   :alt: fig:APIMeasures
   :align: center

   Figure 4: Time stamps captured on the API side.

Figure 3 show how the flow of the KORD API looks like. The execution of the control loop is
triggered either by reception of the Robot Status Heart Beat or timeout of the ``waitSync()``.
The default time out is 8ms (two updates). After a next capture of RSHB the delay between
two consecutive frames is calculated. This delay is then used to calculate jitter.

KORD Round Trip Time
--------------------

Theoretical round trip time is derived from the following time flow diagram and it should be 12ms.

.. figure::  ../images/statistics/RoundTripTime.png
   :alt: fig:RoundTripTime
   :align: center

   Figure 5: Theoretical ideal round trip of the command and its token.

The command is dispatched from the KORD-API and the token travels back to API as a confirmation
of its processing. The command arrives to the KORD CBun and is stored in the receiver thread.
At the start of the next tick, the command is picked up from the receiver thread and passed to the 
processing thread. Then the RSHB is dispatched from the processing thread and the the captured command 
is passed to the controller API. Controller runs with 2ms time slices and therefore the command is
processed in the next tick and its token is passed to the controller API at the end of the controller
tick. Token is picked up by the KORD CBun in its next tick, but since the RSHB is already dispatched,
the token is sent out to the KORD-API in the following tick, accounting for 12 ms in theory. Due to 
additional time currently present in the loop on the CBun side, the time accounts for 16 ms in total.
This is a subject to change in the future. 

Used Internally (Debugging)
===========================

Following statistics are used internally for debugging purposes. They can be 
retrieved from their respective classes as stated in column source. Nevertheless,
their usage for the user is limited and we advice against such usage.


.. list-table:: Table 3: Debug Statistics
   :widths: 20 30 50
   :header-rows: 1

   * - Source
     - Enum Member
     - Description
   * - CBun
     - FAIL_TO_READ_EMPTY
     - Count of instances where 'recvfrom' returned 0. |br|
       It indicates empty read.
   * - CBun
     - FAIL_TO_READ_ERROR
     - Count of instances where 'recvfrom' returned |br| 
       an error.
   * - KORD API
     - MAX_RX_GLOBAL
     - Maximum global difference in API Rx (Receive).
   * - KORD API
     - MIN_RX_GLOBAL
     - Minimum global difference in API Rx (Receive).
   * - KORD API
     - AVG_RX_GLOBAL
     - Average global difference in API Rx (Receive).
   * - KORD API
     - RSHB_LOST_COUNTER_GLOBAL
     - Total number of packets lost during the |br|
       Robot Status Heart Beat frame reception |br|
       over total runtime.
   * - KORD API
     - FAILED_RCV
     - Counter of how many times the 'recvfrom' |br|
       returned error code.
       
For debugging purposes it is possible to print out the statistics from the CBun and KORD API.
Example output may look like the one in Table 4 when helper function `printStatistics()`` is used.

Table 4: Example of statistics printed out on the console.

+----------------------------------+---------------+---------------+---------------+
| Metrics                          | GlobalMax[ms] | GlobalAvg[ms] | GlobalMin[ms] |
+==================================+===============+===============+===============+
| Command Jitter                   | 0.618         |               |               |
+----------------------------------+---------------+---------------+---------------+
| Round Trip Time (RTT)            | 1.834         |               |               |
+----------------------------------+---------------+---------------+---------------+
| System Jitter                    | 0.619         |               |               |
+----------------------------------+---------------+---------------+---------------+
| API's TimeStamps Delay           | 12.1298       | 3.99994       | 3.19517       |
+----------------------------------+---------------+---------------+---------------+
| CBun's TimeStamps Delay          | 4.69851       | 3.99993       | 3.33362       |
+----------------------------------+---------------+---------------+---------------+
| API's TimeStamps Jitter          | 11.4513       | 0.021165      |               |
+----------------------------------+---------------+---------------+---------------+



