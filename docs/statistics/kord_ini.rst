.. _kord_ini:

***************************
Configuration File
***************************

This section regards the content of the KORD.ini file. This file is used to configure
the KORD application. The file is supposed to be imported to the ``workcell`` before
execution.

Required steps to do before transfering the data
================================================

To retrieve a file a few prerequisites need to be met.

1. Upload the `KORD.ini` to the Teach Pendant
---------------------------------------------

The `KORD.ini` is a file for providing information about client PC (current solution is for Linux and MacOS machines only):

Example with the mandatory fields:

.. code-block:: ini

 [KORD]
 username = my_username_on_pc


Generally, this is sufficient information for `KORD`.
However, you can also provide extra parameters, such as `calibration_path` and `host`. 
This might be useful when you used the `KORD` interactively using the Teach Pendant.

.. code-block:: ini

 [KORD]
 username = my_username_on_pc
 calibration_path = ~/kassow_calibration/ ; <- for calibration data
 diagnostics_path = ~/kassow_diagnostics/ ; <- for diagnostics data
 log_path = ~/kassow_logs/ ; <- for logs data
 host     = 192.168.X.Y


For transferring logs ``log_path`` is used, for calibration (KINCAL data) - ``calibration_path`` and for diagnostics (dashboard json file) - ``diagnostics_path``.

2. Add key to your PC
---------------------

You need run the following commands:

.. code-block:: bash

 export pub_key="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCadKOmGzamjx1+FMmMCweVIDCb7NQmYSwnKfeqO9f+StmWvcLZNAYkB5A7UoiuRG+pLzAXAdQCKptEryXdfCvR3BcMAXBJHfAZiAVTesEf5cStZB4KREgE9HOidR4Y1mSheZuVZEFxREQijk6okbad31jdMQRGrjofjBodENDbcRDPdKB8vyi8Cdoq22R5phBV1m0lTsV9kNSO2/74Xsb3i6bTw/fCZhlEW+SDvJpzcizHP+Kt/CWD8jAHDNaVHQdTAgF5QVvlwjxUgBeuM5u5+ZDfDb2QPAS1bhspvQ1IdVCjQOLypDb8Ql2Huh0FNkPdcvUHePIs3qwv5If9O913 nucky@r2d2"
 echo $pub_key >> ~/.ssh/authorized_keys


These two steps are required in order to have the file (urdf file of calibration) transferred.
If statistics limits need to be changed, other sections of the file can be adjusted.

File content
============

.. code-block:: ini
    
    [KORD]
    username = user ; mandatory field
    log_path = ~/kassow_logs/ ; where to transfer logs on your PC
    diagnostics_path = ~/kassow_diagnostics/ ; where to transfer diagnostics data on your PC
    calibration_path = ~/kassow_calibration/ ; where to transfer kincal data on your PC
    host     = 192.168.100.193 ; host IP address
    show_dialogs = yes ; set to "no" to disable dialogs on the teach pendant from showing up, set to "yes" by default

    [QOC_MONITOR]
    ; If following parameters are crossed then a safety event is triggered to stop the robot movement
    stat_window_size                  =    0.2 ; seconds, recent window statistics

    [QOC_HALT_TRIGGER]
    enabled                       =  yes ; enable/disable
    on_consecutive_commands_lost  =    2 ; number of consecutive lost frames to trigger soft stop
    on_recent_commands_lost       =    5 ; number of command request lost in the recent window stat period
    on_recent_avg_system_jitter   =  100 ; microseconds
    on_recent_max_system_jitter   =  500 ; microseconds
    on_recent_avg_roundtrip_time  =  300 ; microseconds
    on_recent_avg_cmd_jitter      =  500 ; microseconds
    on_movej_cmd_ceased_span      = 1e-6 ; radians
    on_movel_cmd_ceased_span      = 1e-6 ; meters

    [IO]
    ; there are 4 possible values for safety digital outputs:
    ; 0 or 1 - disabled (default value)
    ; 2 - enabled
    ; 3 - PStop mapping
    ; 4 - EStop mapping
    ; 5 - P+EStops mapping
    SDO1 = 0 ; disabled
    SDO2 = 5 ; P+EStops mapping
    SDO3 = 3 ; PStop mapping
    SDO4 = 0 ; disabled

The `KORD.ini` is a file for providing information about client PC (current solution is for Linux and MacOS machines only):

Example with the mandatory fields:

.. code-block:: ini

 [KORD]
 username = my_username_on_pc


Generally, this is sufficient information for `KORD`.
However, you can also provide extra parameters, such as `log_path` and `host`. 
This might be useful when you used the `KORD` interactively using the Teach Pendant.

Section KORD
------------
General setup and paths for transferring files (relative paths are also valid).

+---------------------+-------------------------------------------------------------------------------+
| Field               | Description                                                                   |
+=====================+===============================================================================+
| username            | Username of the user device with KORD API (for file transfer using scp)       |
+---------------------+-------------------------------------------------------------------------------+
| log_path            | Path on the user PC with KORD API where logs are transferred                  |
+---------------------+-------------------------------------------------------------------------------+
| diagnostics_path    | Path on the user PC with KORD API where diagnostics files are transferred     |
+---------------------+-------------------------------------------------------------------------------+
| host                | IP address of the user device with KORD API (for file transfer using scp)     |
+---------------------+-------------------------------------------------------------------------------+
| calibration_path    | Path on the user PC with KORD API where calibration files will be transferred |
+---------------------+-------------------------------------------------------------------------------+


Statistics parameters for the CBun
----------------------------------

The following configuration is applied to the statistics captured and processed on the **KORD CBun side only**. 
If you want to configure API statistics parameters, use relevant API calls: :ref:`API Statistics <api-statistics>`.



-------------------
Section QOC_MONITOR
-------------------

General setup for statistics: the window of local statistics can be specified here (for ``on_recent_commands_lost``, ``on_recent_avg_system_jitter``, ``on_recent_max_system_jitter``, ``on_recent_avg_roundtrip_time``, ``on_recent_avg_cmd_jitter`` below).

+---------------------+-------------------------------------------------------------------------------+
| Field               | Description                                                                   |
+=====================+===============================================================================+
| stat_window_size    | Window in seconds for all the recent statistics                               |
+---------------------+-------------------------------------------------------------------------------+

------------------------
Section QOC_HALT_TRIGGER
------------------------
Here detailed statistics parameters can be specified, otherwise the defaults are always used.

+-----------------------------+--------------------------------------------------------------------------------------------------------------------------+
| Field                       | Description                                                                                                              |
+=============================+==========================================================================================================================+
| enabled                     | Enables or disables the QOC_HALT_TRIGGER feature (yes for enable, no for disable)                                        |
+-----------------------------+--------------------------------------------------------------------------------------------------------------------------+
| on_consecutive_commands_lost| Number of consecutive lost frames to trigger a soft stop                                                                 |
+-----------------------------+--------------------------------------------------------------------------------------------------------------------------+
| on_recent_commands_lost     | Number of command requests lost in the recent window stat period                                                         |
+-----------------------------+--------------------------------------------------------------------------------------------------------------------------+
| on_recent_avg_system_jitter | Average system jitter threshold in microseconds                                                                          |
+-----------------------------+--------------------------------------------------------------------------------------------------------------------------+
| on_recent_max_system_jitter | Maximum system jitter threshold in microseconds                                                                          |
+-----------------------------+--------------------------------------------------------------------------------------------------------------------------+
| on_recent_avg_roundtrip_time| Average roundtrip time threshold in microseconds                                                                         |
+-----------------------------+--------------------------------------------------------------------------------------------------------------------------+
| on_recent_avg_cmd_jitter    | Average command jitter threshold in microseconds                                                                         |
+-----------------------------+--------------------------------------------------------------------------------------------------------------------------+
| on_movej_cmd_ceased_span    | Threshold for MoveJ command ceased span in radians (to catch unwanted abrupt changes when performing not discrete moves) |
+-----------------------------+--------------------------------------------------------------------------------------------------------------------------+
| on_movel_cmd_ceased_span    | Threshold for MoveL command ceased span in meters  (to catch unwanted abrupt changes when performing not discrete moves) |
+-----------------------------+--------------------------------------------------------------------------------------------------------------------------+

-----------------------------------
How statistics problems are handled
-----------------------------------

The CBun always notifies the user by showing a pop-up/alarm dialog on the Teach Pendant with a brief description of the problem. 
If QOC_HALT_TRIGGER feature is enabled, the robot is getting suspended after any threshold is exceeded, the following call is used:

.. code-block:: c++

    std::string rc_command = "ctl/suspend"; 
    consent_params.data_ = rc_command.c_str();
    consent_params.data_len_ = rc_command.length();

    program_api_->rc_api_->system_controller_->cmd_UserConsent(consent_params);


To be able to use the moves again, you need to clean **CBun error (parameter --cbun)** (and other alarm states if present) using :doc:`../examples/ex_clean_alarm`.
The relevant description of the system jitter, round trip time, command jitter and more can be found in the :doc:`../statistics/statistics` description. 

----------
Section IO
----------

It is possible to configure safe digital outputs via ``KORD.ini``.

+---------+-----------------------+
| Field   | Description           |
+=========+=======================+
| SDO1    | Safe digital output 1 |
+---------+-----------------------+
| SDO2    | Safe digital output 2 |
+---------+-----------------------+
| SDO3    | Safe digital output 3 |
+---------+-----------------------+
| SDO4    | Safe digital output 4 |
+---------+-----------------------+

There are four possible values for each output:

- 0 or 1 -- disable output,
- 2 -- enable output,
- 3 -- map PStop to the output,
- 4 -- map EStop to the output,
- 5 -- map PStop and EStop to the output.

Example File
============

.. code-block:: ini
    
    [KORD]
    username = user
    log_path = ~/kassow_logs/
    diagnostics_path = ~/kassow_diagnostics/
    calibration_path = ~/kassow_calibration/
    host     = 192.168.100.193
    show_dialogs = yes

    [QOC_MONITOR]
    ; If following parameters are crossed then a safety event is triggered to stop the tobot movement
    stat_window_size                  =    0.2 ;seconds, recent window statistics

    [QOC_HALT_TRIGGER]
    enabled                       =  yes ; enable/disable
    on_consecutive_commands_lost  =    2 ; number of consecutive lost frames to trigger soft stop
    on_recent_commands_lost       =    5 ; number of command request lost in the recent window stat period
    on_recent_avg_system_jitter   =  100 ; microseconds
    on_recent_max_system_jitter   =  500 ; microseconds
    on_recent_avg_roundtrip_time  =  300 ; microseconds
    on_recent_avg_cmd_jitter      =  500 ; microseconds
    on_movej_cmd_ceased_span      = 1e-6 ; radians
    on_movel_cmd_ceased_span      = 1e-6 ; meters

    [IO]
    SDO1 = 0 ; disabled
    SDO2 = 5 ; P+EStops mapping
    SDO3 = 3 ; PStop mapping
    SDO4 = 0 ; disabled
