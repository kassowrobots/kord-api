
.. _ex-set_frame:

Set Frames values
=================

.. note::
    Available in KORD v1.3


It is also possible to set different Frames and Loads using functions:

.. code-block:: c++

    // Sync MUST pass before any data can be fetched.
    kord->syncRC();
    rcv_iface.fetchData();
    
    //
    // Set TCP in reference to TFC
    //
    std::array<double, 6UL> p = {1, 4, 2, 0, 0, 0};
    int64_t token; // token of the command to check its status
    ctl_iface.setFrame(kord::EFrameID::TCP_FRAME, p, kord::EFrameValue::POSE_VAL_REF_TFC, token);
    
    // waiting for the command status-result
    while(rcv_iface.getCommandStatus(token) == -1){
        std::cout << "not found yet \n";
        if (!kord->waitSync(std::chrono::milliseconds(20))){
            std::cout << "Sync wait timed out, exit \n";
            break;
        }
        rcv_iface.fetchData(); // always fetching to see new values
    }

    // Review its values and evaluate them
