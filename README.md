# KORD API
**KORD version 1.4.5**

This project is a client-side API for KORD (Kassow Open Realtime Data). It aims to provide an abstraction layer for real-time access to Kassow Robots' RC (Robot Controller) and its control loop.

Please note that this project is currently in development, and additional information will be provided soon. Stay tuned for updates.

# Master CBun
To enable the KORD API to function on external systems, the Master module must be installed and activated on the RC. This module is essential for establishing connectivity and accessing the RC software.

To install the Master CBun, download it from the [provided link](https://gitlab.com/kassowrobots/kord-api/-/wikis/Master-CBun), copy it onto a USB drive, and then use the CBun Manager to install it on the Kassow Robots RC.

Please note the module contains the actual source code, so you can modify it for your own purposes. To do so rename the 'cbun' extension to 'tar.gz' and extract the content.

## Clone repository including submodules

If you would like to clone the repository with default kord protocol clone with `--recurse-submodules` option.

```bash
$ git clone --recurse-submodules -b v1.4.5  https://gitlab.com/kassowrobots/kord-api.git
```

## Compilation

> Please note that the example source files within the module are dependent on some external libraries, specifically Eigen3 and ncurses.
> To install on Ubuntu/Debian, use: `sudo apt install libncurses-dev libeigen3-dev`.

Run the following code to clone and compile the project with examples.

```bash
cd kord-api
cmake -S . -B build -DKORD_WITH_EXAMPLES=1
cmake --build build
```

Environment variables:
- `KORD_WITH_EXAMPLES` - compile with examples. Disabled by default.
- `KORD_WITH_TESTS` - compile with tests. Disabled by default.
- `KORD)WITH_COVERAGE` - compile with flags necessary for coverage calculation. Disabled by default.
- `GCOV_EXECUTABLE` - `gcov` executable. The version should be equal to compiler's one.
- `COVERAGE_OUTPUT_DIR` - directory name where to put coverage HTML report. Defaults to `coverage`.

Example of usage:
```bash
cmake -S . -B build -DKORD_WITH_EXAMPLES=1 \
                    -DKORD_WITH_TESTS=1 \
                    -DKORD_WITH_COVERAGE=1 \
                    -DGCOV_EXECUTABLE=/usr/bin/gcov-8 \
                    -DCOVERAGE_OUTPUT_DIR=coverage_report
cmake --build build -- -j $(nproc)
```

## Building Debian packages

After the build succeeds, you can build debian packages for library and examples. You must install
the library in order to use the examples. To build the packages enter the build dir and run:

```bash
make package
```

or use cpack:

```bash
cpack -G DEB
```

### Available commands

> Please make sure to run tests prior to generating coverage, otherwise `.gcda` files would be empty.
> 
`make coverage` - generates coverage and places it in `COVERAGE_OUTPUT_DIR` directory. 

## Documentation

Detailed documentation for the latest KORD API (v1.4.5) is available on the following public link:

- [Documentation - KORD API v1.4.5](https://kassowrobots.gitlab.io/kord-api-doc)

For older versions please refer to following links:
- [Documentation - KORD API v1.4.4](https://kassowrobots.gitlab.io/kord-api-doc/versions/1.4.4)

- [Documentation - KORD API v1.4.1](https://kassowrobots.gitlab.io/kord-api-doc/versions/1.4.1)

- [Documentation - KORD API v1.4.0](https://kassowrobots.gitlab.io/kord-api-doc/versions/1.4.0)

- [Documentation - KORD API v1.3.0](https://kassowrobots.gitlab.io/kord-api-doc/versions/1.3.0)

- [Documentation - KORD API v1.2.0](https://kassowrobots.gitlab.io/kord-api-doc/versions/1.2.0/)
